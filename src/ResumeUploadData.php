<?php

namespace Drupal\filefield_sources_resumable;

/**
 * ResumeUploadData short summary.
 *
 * ResumeUploadData description.
 *
 * @version 1.0
 * @author David
 */
class ResumeUploadData {

  public function __construct() {
    // Resumable.js uses GET requests to check if parts of file already
    // exist on server, and if not then issues a POST. In both cases,
    // it send information about the UPLOAD.
    $this->resumableChunkNumber = $_REQUEST['resumableChunkNumber'];
    $this->resumableChunkSize = $_REQUEST['resumableChunkSize'];
    $this->resumableTotalSize = $_REQUEST['resumableTotalSize'];
    $this->resumableIdentifier = $_REQUEST['resumableIdentifier'];
    $this->resumableFilename = $_REQUEST['resumableFilename'];
    $this->resumableRelativePath = $_REQUEST['resumableRelativePath'];
  }

  //The index of the chunk in the current upload. First chunk is 1 (no base�0 counting here).
  public $resumableChunkNumber;
  // The general chunk size. Using this value and resumableTotalSize you can calculate the total number of chunks. Please note that the size of the data received in the HTTP might be lower than resumableChunkSize of this for the last chunk for a file.
  public $resumableChunkSize;
  // The total file size.
  public $resumableTotalSize;
  // A unique identifier for the file contained in the request.
  public $resumableIdentifier;
  // The original file name (since a bug in Firefox results in the file name not being transmitted in chunk multipart posts).
  public $resumableFilename;
  // The file�s relative path when selecting a directory (defaults to file name in all browsers except Chrome).
  public $resumableRelativePath;
  
  private function DestinationDirectory($temporary = TRUE) {
    global $user;
    // We need to prevent file collision between users, each
    // user will have it's own folder for temporary uploads.
    // Anonymous users will only be able to keep data
    // as long as they keep the same sesion.
    $user_folder = $user->uid > 0 ? $user->uid : ('anonymous' . session_id());
    $folder = $temporary == TRUE ? 'temp' : 'files';
    return 'temporary://resumable_files/' . $folder . '/' . $user_folder . md5($this->resumableIdentifier); 
  }
  
  /**
   * Delete stuck uploads.
   */
  public static function CleanOldFiles() {
    $dir = 'temporary://resumable_files/temp/';
    $directories = glob(drupal_realpath($dir) . '/*' , GLOB_ONLYDIR);
    foreach($directories as $directory) {
      $last_write = filemtime($directory . '/.');
      if (time() - $last_write > (3600 * 3600 * 2)) {
        file_unmanaged_delete_recursive($directory);
      }
    }
  }
  
  /**
   * Temporary destination for the current completed file.
   */
  public function DestinationFile($temporary = TRUE) {
    $temp_dir = $this->DestinationDirectory($temporary);
    file_prepare_directory($temp_dir, FILE_CREATE_DIRECTORY);
    $dest_file = $temp_dir . '/' . ($temporary == FALSE ? time() : '') . $this->resumableFilename;
    return $dest_file;
  }
  
  /**
   * Temporary destination for the current chunk.
   */
  public function ChunkDestinationFile() {
    return $this->DestinationFile() . '.part' . $this->resumableChunkNumber;
  }
  
  /**
   *
   * Check if all the parts exist, and 
   * gather all the parts of the file together
   * @param string $dir - the temporary directory holding all the parts of the file
   * @param string $fileName - the original file name
   * @param string $chunkSize - each chunk size (in bytes)
   * @param string $totalSize - original file size (in bytes)
   */
  public function createFileFromChunks() {
    global $user;
    
    // Count all parts of file and sizes.
    $temp_dir = $this->DestinationDirectory();
    $total_size = 0;
    $total_files = 0;
    foreach(file_scan_directory($temp_dir, '*.*') as $file) {
      if ($file->name === $this->resumableFilename) {
        $total_files++;
        $realpath = drupal_realpath($file->uri);
        $size = filesize($realpath);
        $total_size += $size;
      }
    }
    
    // Something is wrong!
    if ($total_size != $this->resumableTotalSize) {
      return FALSE;
    }
    
    // Ask for a non temporary destination for this file.
    $target = $this->DestinationFile(FALSE);
    $target_real = drupal_realpath($target);
    
    // create the final destination file 
    if (($fp = fopen($target_real, 'w')) !== FALSE) {
      for ($i = 1; $i <= $total_files; $i++) {
        $path = $temp_dir . '/' . $this->resumableFilename . '.part' . $i;
        $realpath = drupal_realpath($path);
        fwrite($fp, file_get_contents($realpath));
      }
      fclose($fp);
    }
    else {
      return FALSE;
    }
    
    // Delete all the pieces!
    file_unmanaged_delete_recursive(drupal_realpath($this->DestinationDirectory(TRUE)));
    
    // Create a file object, to get a fid.
    $file = new \stdClass;
    $file->uid = $user->uid;
    $file->filename = $this->resumableFilename;
    $file->uri = $target;
    $file->filemime = mime_content_type($target_real);
    $file->filesize = filesize($target_real);
    $file->status = 0;

    $file = file_save($file);
    return $file;
  }
  
}
