<?php

namespace Drupal\filefield_sources_resumable;

/**
 * ResumeUploadManager short summary.
 *
 * ResumeUploadManager description.
 *
 * @version 1.0
 * @author David
 */
class ResumeUploadManager
{

  public static function resumable_menu_action_menu () {
    if ($_POST['action'] == 'complete') {
      $resumableData = new ResumeUploadData();
      $file = $resumableData->createFileFromChunks();
      drupal_json_output($file);
      drupal_exit();
    }
  }

  public static function resumable_menu_action_access () {
    return TRUE;
  }
  
  /**
   * Verifies the token for this request.
  NEEDS IMPROVMENT!
   */
  public static function resumable_upload_access() {
    if (!drupal_valid_token($_REQUEST['token'], 'filefield_sources_resumable')) {
      return FALSE;
    }
    //check if request is GET and the requested chunk exists or not. this makes testChunks work
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $resumableData = new ResumeUploadData();
      // This will allow uploads to be resumed after browser restarts and even across browsers 
      // (in theory you could even run the same file upload across multiple tabs or different browsers). 
      // The POST data requests listed are required to use Resumable.js to receive data, but you can 
      // extend support by implementing a corresponding GET request with the same parameters:
      // If this request returns a 200 HTTP code, the chunks is assumed to have been completed.
      // If the request returns anything else, the chunk will be uploaded in the standard fashion.
      $dest_file = $resumableData->ChunkDestinationFile();
      $dest_path = drupal_realpath($dest_file);
      if (!file_exists($dest_path)) {
        drupal_add_http_header('Status', '404 Not Found');
        drupal_fast_404();
        return FALSE;
      } 
    } 
    else {
      foreach (func_get_args() as $permission) {
        if (!user_access($permission)) {
          drupal_add_http_header('Status', '404 Not Found');
          drupal_fast_404();
          return FALSE;
        }
      }
    }
    return TRUE;
  }


  /**
   * Callback that handles and saves uploaded files.
   *
   * This will respond to the URL on which plupoad library will upload files.
   */
  public static function resumable_handle_upload() {
    $resumableData = new ResumeUploadData();
    
    // loop through files and move the chunks to a temporarily created directory
    if (!empty($_FILES)) { 
      foreach ($_FILES as $file) {
        // check the error status
        if ($file['error'] != 0) {
          _log('error '. $file['error'] .' in file '. $resumableData->resumableFilename);
          continue;
        }

        // init the destination file (format <filename.ext>.part<#chunk>
        // the file is stored in a temporary directory
        $dest_file = $resumableData->ChunkDestinationFile();

        // move the temporary file
        if (!drupal_move_uploaded_file($file['tmp_name'], $dest_file)) {
          _log('Error saving (move_uploaded_file) chunk '. $resumableData->resumableChunkNumber .' for file '. $resumableData->resumableFilename);
        }
      }
    }
  }
}
