<div class="resumable">
    <div class="resumable-error">
        Your browser, unfortunately, is not supported by Resumable.js. The library requires support for <a href="http://www.w3.org/TR/FileAPI/">the HTML5 File API</a> along with <a href="http://www.w3.org/TR/FileAPI/#normalization-of-params">file slicing</a>.
    </div>
    <div class="resumable-drop">
        <?php print t('Drop files here to upload')?>
        <br />
        <a class="resumable-browse"><u><?php print t('Select from your computer')?></u></a>
    </div>
    <div class="resumable-progress">
        <div>
            <div id="progrestest" style="display: inline-block; width: 70%;">
                <div class="progress-label">Loading...</div>
            </div>
            <div class="progress-pause" style="display: inline-block;">
                <a class="progress-resume-link">
                    <img src="<?php print base_path() . path_to_theme(); ?>/theme/resume.png" title="Resume upload" /></a>
                <a class="progress-pause-link">
                    <img src="<?php print base_path() . path_to_theme(); ?>/theme/pause.png" title="Pause upload" /></a>
            </div>
        </div>
    </div>
</div>
