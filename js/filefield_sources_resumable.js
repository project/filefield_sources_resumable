(function ($) {

    Drupal.resumable = Drupal.resumable || {};

    /**
     * Attaches the Plupload behavior to each FileField Sources Plupload form element.
     */
    Drupal.behaviors.filefield_sources_resumable = {
        attach: function (context, settings) {

            var BytesToHumanReadable = function (bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return 'n/a';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            };

            $.each($('.filefield-source.filefield-source-resumable'), function (index, resumable) {

                var r = Resumable({
                    target: settings.basePath + 'resumable-handle-uploads?token=' + settings.fileFieldSourcesResumable.token,
                    chunkSize: 1 * 1024 * 1024,
                    simultaneousUploads: 1,
                    prioritizeFirstAndLastChunk: true,
                    testChunks: true,
                    throttleProgressCallbacks: 1,
                    maxFiles: 1,
                });

                var resumableAction = function (action, file) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        data: {
                            action: action,
                            resumableTotalSize: file.size,
                            resumableIdentifier: file.uniqueIdentifier,
                            resumableFilename: file.fileName,
                            resumableRelativePath: file.relativePath,
                        },
                        url: settings.basePath + 'resumable-handle-actions?token=' + settings.fileFieldSourcesResumable.token,
                        success: function (data, textStatus, jqXHR) {
                            $('input[type=hidden]', resumable).val(data.fid);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });
                };

                // Resumable.js isn't supported, fall back on a different method
                if (!r.support) {
                    $('.resumable-error', resumable).show();
                } else {
                    // Show a place for dropping/selecting files
                    $('.resumable-drop', resumable).show();
                    r.assignDrop($('.resumable-drop', resumable).first());
                    r.assignBrowse($('.resumable-browse', resumable).first());

                    // Handle file add event
                    r.on('fileAdded', function (file) {
                        $('.resumable-drop', resumable).hide();
                        // Show progress pabr
                        $('.resumable-progress, .resumable-list', resumable).show();
                        // Show pause, hide resume
                        $('.resumable-progress .progress-resume-link', resumable).hide();
                        $('.resumable-progress .progress-pause-link', resumable).show();
                        // Add the file to the list
                        $('.resumable-list', resumable).append('<li class="resumable-file-' + file.uniqueIdentifier + '">Uploading <span class="resumable-file-name"></span> <span class="resumable-file-progress"></span>');
                        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-name', resumable).html(file.fileName);
                        // TODO:Calculate optimum chunk size. Depends of file size of upload.
                        r.chunkSize = (1024 * 100);
                        // Actually start the upload
                        r.upload();
                    });
                    r.on('pause', function () {
                        // Show resume, hide pause
                        $('.resumable-progress .progress-resume-link', resumable).show();
                        $('.resumable-progress .progress-pause-link', resumable).hide();
                    });
                    r.on('complete', function () {
                        // Hide pause/resume when the upload has completed
                        $('.resumable-progress .progress-resume-link, .resumable-progress .progress-pause-link', resumable).hide();
                        // Time to post, attach unique ID to the form element.
                        $.each(r.files, function (index, file) {
                            resumableAction('complete', file);
                        });

                    });
                    r.on('fileSuccess', function (file, message) {
                        // Reflect that the file upload has completed
                        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress', resumable).html('(completed)');
                    });
                    r.on('fileError', function (file, message) {
                        // Reflect that the file upload has resulted in error
                        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress', resumable).html('(file could not be uploaded: ' + message + ')');
                    });
                    r.on('fileProgress', function (file) {
                        // Handle progress for both the file and the overall upload
                        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress', resumable).html(Math.floor(file.progress() * 100) + '%');
                        $('#progrestest', resumable).progressbar({ value: r.progress() * 100 });
                        // Speed and actual progress
                        var detail = BytesToHumanReadable(r.getSize() * r.progress()) + '/' + BytesToHumanReadable(r.getSize());
                        $('.progress-label', resumable).text(Math.floor(r.progress() * 100) + '% ' + '[' + detail + ']');
                    });

                    $('#progrestest', resumable).progressbar({ value: 0 });

                    $('.resumable-progress .progress-resume-link', resumable).click(
                        function () {
                            $('.resumable-progress .progress-resume-link', resumable).hide();
                            $('.resumable-progress .progress-pause-link', resumable).show();
                            r.upload();
                            return false;
                        });
                    $('.resumable-progress .progress-pause-link', resumable).click(function () { r.pause(); return false; });
                }
            });
        }
    }
})(jQuery);




